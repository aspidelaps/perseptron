public class neuron {

    private Integer[] weights = new Integer[12];

    public neuron(){
       for(int i = 0; i < weights.length; i++){
           weights[i] = 0;
       }
    }

    public Integer[] getWeights() {
        return weights;
    }

    public void setWeights(Integer[] i){
        System.arraycopy(i, 0, weights, 0, 12);
    }

    public int calculateOutput(Integer[] a, Integer O){
        Integer s = 0;
        for (int i = 0; i < weights.length; i++){
            s = s + (weights[i] * a[i]);
        }
        if(s >= O){
            return 1;
        }
        else{
            return 0;
        }
    }

    public void increaseWeights(Integer[] a){
        for (int i = 0; i < weights.length; i++){
            weights[i] = weights[i] + a[i];
        }
    }

    public void reduceWeights(Integer[] a){
        for (int i = 0; i < weights.length; i++){
            weights[i] = weights[i] - a[i];
        }
    }

}
