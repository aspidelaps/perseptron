import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class main {
    final static Integer O = 0;

    public static void main(String[] args) {
        int isEven;
        int outputValue;
        int count = 0;
        neuron n = new neuron();
        for (int i = 0; i < 10; i++) {
            count++;
            Integer[] array = convert(i);
            isEven = isEven(i);
            outputValue = n.calculateOutput(array, O);
            if (isEven == outputValue) {
                continue;
            } else {
                if (outputValue == 0) {
                    n.increaseWeights(array);
                    i = -1;
                } else {
                    n.reduceWeights(array);
                    i = -1;
                }
            }
        }
        System.out.println(count);
        try (final FileWriter writer = new FileWriter("C:\\Users\\Mark\\IdeaProjects\\perseptron\\src\\weights.txt", false)) {
            Integer[] array = n.getWeights();
            for (int i = 0; i < array.length; ++i) {
                final String s = Integer.toString(array[i]);
                writer.write(s);
                writer.write(System.lineSeparator());
                System.out.println(s);
            }
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
        boolean flag = true;
        while (flag){
            System.out.println("Enter your number:");
            Scanner in = new Scanner(System.in);
            int test = in.nextInt();
            if((test > 10)||(test < 0)){
                System.out.println("Wrong number");
                continue;
            }
            /*if(test == 0){
                flag = false;
                System.out.println("Goodbye");
            }*/
            else{
                if(n.calculateOutput(convert(test), O) == 0){
                    System.out.println("Number " + test + " not even");
                }
                if(n.calculateOutput(convert(test), O) == 1){
                    System.out.println("Number " + test + " is even");
                }
            }
        }
    }

    //Конвертер integer в массив
    public static Integer[] convert(int i) {
        if (i == 0) {
            Integer[] result = {1, 1, 1, 1, 0, 1, 1, 0, 1, 1, 1, 1};
            return result;
        }
        if (i == 1) {
            Integer[] result = {0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0};
            return result;
        }
        if (i == 2) {
            Integer[] result = {1, 1, 1, 0, 0, 1, 0, 1, 0, 1, 1, 1};
            return result;
        }
        if (i == 3) {
            Integer[] result = {1, 1, 1, 0, 1, 0, 0, 0, 1, 1, 1, 1};
            return result;
        }
        if (i == 4) {
            Integer[] result = {1, 0, 1, 1, 0, 1, 1, 1, 1, 0, 0, 1};
            return result;
        }
        if (i == 5) {
            Integer[] result = {1, 1, 1, 1, 1, 0, 0, 0, 1, 1, 1, 1};
            return result;
        }
        if (i == 6) {
            Integer[] result = {1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1};
            return result;
        }
        if (i == 7) {
            Integer[] result = {1, 1, 1, 0, 1, 0, 1, 0, 0, 1, 0, 0};
            return result;
        }
        if (i == 8) {
            Integer[] result = {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1};
            return result;
        }
        if (i == 9) {
            Integer[] result = {1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1};
            return result;
        }
        return null;
    }

    public static int isEven(Integer i) {
        if (i % 2 == 0) {
            return 1;
        } else {
            return 0;
        }
    }
}
/*try {
            Scanner sc = new Scanner(new File("C:\\Users\\Mark\\IdeaProjects\\perseptron\\src\\test.txt"));
        } catch (FileNotFoundException e) {
            System.out.println("Такого файла нет");
        }
        catch (NoSuchElementException e) {
            System.out.println("Тренировочный файл закончился");
        }*/